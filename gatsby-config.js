module.exports = {
  siteMetadata: {
    title: 'Guitar Chords Sound Board',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    'gatsby-plugin-react-next'
  ],
}
