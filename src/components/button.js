import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Button extends Component {
  handleClick = e => {
    const { target: element } = e
    const { sendToParent } = this.props
    if (element.classList.contains('icon')) {
      return
    } else if (element.tagName === 'SPAN' || element.tagName === 'SMALL') {
      const dataset = element.parentNode.dataset
      return sendToParent({
        audio: true, //flag for parent function
        dataset: dataset.audio,
      })
    }
    sendToParent({ audio: true, dataset: e.target.dataset.audio })
  }
  //play audio

  toggleInfoBox = () => {
    this.props.sendToParent({
      props: true, //flag for parent function
      chord: this.props.chord,
      fingers: this.props.fingers,
      show: true,
      frets: this.props.frets,
    })
  }

  render() {
    const { chord, Key, shift, dataset } = this.props
    return (
      <button
        onClick={this.handleClick}
        type="button"
        className={`btn btn--${chord}`}
        data-key={Key}
        data-audio={dataset}
      >
        <span onClick={this.toggleInfoBox} className="icon">
          i
        </span>

        <span className="chordName">{chord}</span>

        <small className="key">{shift ? `⇧ + ${Key}` : Key}</small>
      </button>
    )
  }
}
Button.propTypes = {
  chord: PropTypes.string,
  Key: PropTypes.string,
  shift: PropTypes.bool,
  dataset: PropTypes.number,
  show: PropTypes.bool,
  sendToParent: PropTypes.func,
  fingers: PropTypes.array,
  frets: PropTypes.array,
}
export default Button
