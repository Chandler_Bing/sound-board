import React, { Component } from 'react'
import '../layouts/chordInfo.sass'
import Finger from './finger'
import PropTypes from 'prop-types'
class ChordInfo extends Component {
  render() {
    const strings = ['E', 'A', 'D', 'G', 'B', 'e']
    const { chord, frets, show, fingers, children } = this.props
    return (
      <div
        ref={this.props.index}
        className={`chord-info ${show ? 'show' : ''}`}
      >
        <em className="chordName"> {chord}</em>
        {children}
        <div className="guitar">
          <div className="fretboard">
            {frets.map(f => (
              <div key={`fret-${f}`} className={`fret fret-${f}`} data-fret={f}>
                {strings.map((s, i) => (
                  <div key={`string-${s}`} className={`string string-${s}`}>
                    <Finger
                      key={`finger-${i}`}
                      fs={`${f}-${s}`}
                      fingers={fingers}
                    />
                  </div>
                ))}
              </div>
            ))}
          </div>
          <ul className="legend">
            <li className="title">
              <h2>Legend</h2>
            </li>
            <li>
              <span>1</span> - index finger
            </li>
            <li>
              <span>2</span> - middle finger
            </li>
            <li>
              <span>3</span> - ring finger
            </li>
            <li>
              <span>4</span> - pinky finger
            </li>
            <li>
              <span>x</span> - mute string
            </li>
            <li>side numbers - frets</li>
          </ul>
        </div>
      </div>
    )
  }
}
ChordInfo.propTypes = {
  chord: PropTypes.string,
  frets: PropTypes.array,
  show: PropTypes.bool,
  fingers: PropTypes.array,
}
export default ChordInfo
