import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
class Finger extends Component {
  render() {
    const { fingers } = this.props
    const [fret, string] = this.props.fs.split('-')

    return (
      <Fragment>
        {fingers.map(finger => {
          if (finger.fret == fret && finger.string == string) {
            return (
              <div key={finger} className="finger">
                <span>{finger.finger}</span>
              </div>
            )
          }
        })}
      </Fragment>
    )
  }
}
Finger.propTypes = {
  fingers: PropTypes.array,
  fs: PropTypes.string,
}
export default Finger
