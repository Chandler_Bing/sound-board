import Amajor from '../audio/A.mp3'
import Aminor from '../audio/Am.mp3'
import Bminor7 from '../audio/Bm7.mp3'
import Cmajor from '../audio/C.mp3'
import Dmajor from '../audio/D.mp3'
import Emajor from '../audio/E.mp3'
import Eminor from '../audio/Em.mp3'
import Fmajor from '../audio/F.mp3'
import FsharpMinor7 from '../audio/Fsharp_m7.mp3'
import Gmajor from '../audio/G.mp3'
const chords = [
  {
    //dataset is the keycode of keyboard key that will trigger the sound.
    audio: { file: Amajor, dataset: 97 },
    frets: [1, 2, 3, 4],
    name: 'A',
    key: 'A',
    shift: false,
    firstFret: 1,
    fingers: [
      { finger: 'x', fret: 2, string: 'E' },
      { finger: 1, fret: 2, string: 'B' },
      { finger: 2, fret: 2, string: 'D' },
      { finger: 3, fret: 2, string: 'G' },
    ],
  },
  {
    audio: { file: Aminor, dataset: 65 },
    frets: [1, 2, 3, 4],
    name: 'Am',
    key: 'A',
    shift: true,
    firstFret: 1,
    fingers: [
      { finger: 'x', fret: 2, string: 'E' },
      { finger: 1, fret: 1, string: 'B' },
      { finger: 2, fret: 2, string: 'D' },
      { finger: 3, fret: 2, string: 'G' },
    ],
  },
  {
    audio: { file: Bminor7, dataset: 66 },
    frets: [1, 2, 3, 4],
    name: 'Bm7',
    key: 'B',
    shift: true,
    firstFret: 7,
    fingers: [
      { finger: 'x', fret: 2, string: 'E' },
      { finger: 2, fret: 2, string: 'A' },
      { finger: 3, fret: 2, string: 'G' },
      { finger: 4, fret: 2, string: 'e' },
    ],
  },
  {
    audio: { file: Cmajor, dataset: 99 },
    frets: [1, 2, 3, 4],
    name: 'C',
    key: 'C',
    shift: false,
    firstFret: 1,
    fingers: [
      { finger: 'x', fret: 2, string: 'E' },
      { finger: 1, fret: 1, string: 'B' },
      { finger: 2, fret: 2, string: 'D' },
      { finger: 3, fret: 3, string: 'A' },
    ],
  },
  {
    audio: { file: Dmajor, dataset: 100 },
    frets: [1, 2, 3, 4],
    name: 'D',
    key: 'D',
    shift: false,
    firstFret: 1,
    fingers: [
      { finger: 'x', fret: 2, string: 'E' },
      { finger: 'x', fret: 2, string: 'A' },
      { finger: 1, fret: 2, string: 'G' },
      { finger: 2, fret: 2, string: 'e' },
      { finger: 3, fret: 3, string: 'B' },
    ],
  },
  {
    audio: { file: Emajor, dataset: 101 },
    frets: [1, 2, 3, 4],
    name: 'E',
    key: 'E',
    shift: false,
    firstFret: 1,
    fingers: [
      { finger: 1, fret: 1, string: 'G' },
      { finger: 2, fret: 2, string: 'D' },
      { finger: 3, fret: 2, string: 'A' },
    ],
  },
  {
    audio: { file: Eminor, dataset: 69 },
    frets: [1, 2, 3, 4],
    name: 'Em',
    key: 'E',
    shift: true,
    firstFret: 1,
    fingers: [
      { finger: 2, fret: 2, string: 'D' },
      { finger: 3, fret: 2, string: 'A' },
    ],
  },
  {
    audio: { file: Fmajor, dataset: 102 },
    frets: [1, 2, 3, 4],
    name: 'F',
    key: 'F',
    shift: false,
    firstFret: 1,
    fingers: [
      { finger: 1, fret: 1, string: 'e' },
      { finger: 1, fret: 1, string: 'B' },
      { finger: 2, fret: 2, string: 'G' },
      { finger: 3, fret: 2, string: 'D' },
    ],
  },
  {
    audio: { file: FsharpMinor7, dataset: 70 },
    frets: [1, 2, 3, 4],
    name: 'F#m7',
    key: 'F',
    shift: true,
    firstFret: 7,
    fingers: [
      { finger: 2, fret: 2, string: 'E' },
      { finger: 'x', fret: 2, string: 'A' },
      { finger: 3, fret: 2, string: 'D' },
      { finger: 3, fret: 2, string: 'G' },
      { finger: 3, fret: 2, string: 'B' },
      { finger: 'x', fret: 2, string: 'e' },
    ],
  },
  {
    audio: { file: Gmajor, dataset: 103 },
    frets: [1, 2, 3, 4],
    name: 'G',
    key: 'G',
    shift: false,
    firstFret: 1,
    fingers: [
      { finger: 2, fret: 1, string: 'A' },
      { finger: 3, fret: 2, string: 'E' },
      { finger: 4, fret: 2, string: 'e' },
    ],
  },
]
export default chords
