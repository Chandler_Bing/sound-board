import React, { Component } from 'react'
import Button from '../components/button'
import '../layouts/button.sass'
import chords from '../data/chords'
import ChordInfo from '../components/chordInfo'
import defaultAudio from '../audio/default.mp3'
class IndexPage extends Component {
  state = {
    chord: '',
    fingers: [],
    frets: [],
    show: false,
  }
  // receive info from child and decide what to do with it depending on the flag
  getInfoFromChild = info => {
    if (info.props) {
      const { chord, fingers, show, frets } = info
      this.setState({ chord, fingers, show, frets })
    } else if (info.audio) {
      const { dataset } = info
      this.handleKeyPress({ keyCode: dataset })
    }
  }
  closeChordInfo = () => {
    this.setState({ show: false })
  }
  handleKeyPress = e => {
    const { keyCode: key } = e
    const button = document.querySelector(`[data-audio="${key}"]`)
    const audiofile = this.refs[key] || this.defaultAudio
    button && e.target && button.classList.add('active')
    /*
Fixes bug when someone plays the sound with keyboard shortcut and hovers over the same button with a mouse, the active class doesn't get removed.
*/
    button &&
      audiofile.addEventListener('ended', () =>
        button.classList.remove('active')
      )
    audiofile.currentTime = 0
    audiofile.play()
  }
  componentDidMount() {
    window.addEventListener('keypress', this.handleKeyPress)
  }
  render() {
    return (
      <div className="container">
        <div className="info-container">
          <ChordInfo
            chord={this.state.chord}
            show={this.state.show}
            fingers={this.state.fingers}
            frets={this.state.frets}
          >
            <button
              onClick={this.closeChordInfo}
              onMouseOut={e => e.target.classList.remove('active')}
              onMouseOver={e => e.target.classList.add('active')}
              className="close"
            />
          </ChordInfo>
        </div>
        <div className="btn-container">
          {chords.map(chord => (
            <Button
              dataset={chord.audio.dataset}
              show={this.state.show}
              sendToParent={this.getInfoFromChild}
              key={chord.name}
              chord={chord.name}
              Key={chord.key}
              shift={chord.shift}
              fingers={chord.fingers}
              frets={chord.frets}
            />
          ))}
        </div>
        {chords.map(chord => (
          <audio
            key={`audio-${chord.name}`}
            src={chord.audio.file}
            ref={chord.audio.dataset}
          />
        ))}
        <audio src={defaultAudio} ref={input => (this.defaultAudio = input)} />
      </div>
    )
  }
}

export default IndexPage
